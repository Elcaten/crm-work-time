﻿using System;

namespace Lineyschikov.Andrew.CrmWorkTime
{
    internal struct Constants
    {
        public struct Projects
        {
            public const string GMCS = "GMCS";
        }
        public struct ProjectItems
        {
            public const string DownTime = "Простой";
        }

        public struct Hours
        {
            public const int Start = 10;
            public const int Shutdown = 19;
        }
    }
}