﻿using System;
using System.Collections.Generic;

namespace Lineyschikov.Andrew.CrmWorkTime
{
    /// <summary>
    /// Сравнивает double с заданной точностью
    /// </summary>
    internal class PrecisionComparer : IEqualityComparer<double>
    {
        private static double _epsilon;

        public PrecisionComparer(double epsilon = 0.0001)
        {
            _epsilon = epsilon;
        }

        public bool Equals(double x, double y)
        {
            double diff = Math.Abs(x - y);
            if (x == y) return true;
            if (x == 0 || y == 0 || diff < double.Epsilon) return diff < _epsilon;
            return diff / (Math.Abs(x) + Math.Abs(y)) < _epsilon;
        }

        public int GetHashCode(double obj)
        {
            return obj.GetHashCode();
        }
    }
}