using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using LibGit2Sharp;

namespace Lineyschikov.Andrew.CrmWorkTime
{
    /// <summary>
    /// Сравнивает коммиты по сообщениям
    /// </summary>
    internal class CommitMessageComparer : IEqualityComparer<Commit>
    {
        public bool Equals(Commit x, Commit y)
        {
            var x1 = RemoveWhitespaces(x.Message);
            var y1 = RemoveWhitespaces(y.Message);
            return x1.Equals(y1, StringComparison.OrdinalIgnoreCase);
        }

        public int GetHashCode(Commit obj)
        {
            return obj.Message.GetHashCode();
        }

        private static string RemoveWhitespaces(string str)
        {
            return Regex.Replace(str, @"\s", "");
        }
    }
}