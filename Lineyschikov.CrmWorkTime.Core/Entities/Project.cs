﻿using System;
using System.Runtime.Serialization;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;

namespace Lineyschikov.Andrew.CrmWorkTime.Entities
{
    [DataContract]
    [EntityLogicalName("new_project")]
    public class Project : Entity
    {
        public struct Fields
        {
            public const string CreatedBy = "createdby";
            public const string CreatedOn = "createdon";
            public const string CreatedOnBehalfBy = "createdonbehalfby";
            public const string ImportSequenceNumber = "importsequencenumber";
            public const string ModifiedBy = "modifiedby";
            public const string ModifiedOn = "modifiedon";
            public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
            public const string NewMppPath = "new_mpp_path";
            public const string NewName = "new_name";
            public const string NewProjectId = "new_projectid";
            public const string Id = "new_projectid";
            public const string NewStatus = "NewStatus";
            public const string OrganizationId = "organizationid";
            public const string OverriddenCreatedOn = "overriddencreatedon";
            public const string Statecode = "Statecode";
            public const string Statuscode = "Statuscode";
            public const string TimeZoneRuleVersionNumber = "timezoneruleversionnumber";
            public const string UtcConversionTimeZoneCode = "utcconversiontimezonecode";
            public const string VersionNumber = "versionnumber";
        }

        internal const string EntityLogicalName = "new_project";

        public Project() : base(EntityLogicalName)
        {
        }

        /// <summary>
        /// Название проекта
        /// </summary>
        [AttributeLogicalName("new_name")]
        public string Name
        {
            get { return GetAttributeValue<string>("new_name"); }
            set { SetAttributeValue("new_name", value); }
        }

        /// <summary>
        /// Уникальный идентификатор экземпляров сущности
        /// </summary>
        [AttributeLogicalNameAttribute("new_projectid")]
        public Guid? NewProjectId
        {
            get { return GetAttributeValue<Guid?>("new_projectid"); }
            set
            {
                SetAttributeValue("new_projectid", value);
                base.Id = value ?? Guid.Empty;
            }
        }

        [AttributeLogicalNameAttribute("new_projectid")]
        public override Guid Id
        {
            get { return base.Id; }
            set { NewProjectId = value; }
        }
    }
}