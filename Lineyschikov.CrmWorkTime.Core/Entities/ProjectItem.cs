﻿using System;
using System.Runtime.Serialization;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;

namespace Lineyschikov.Andrew.CrmWorkTime.Entities
{
    [DataContract]
    [EntityLogicalName("new_project_item")]
    public class ProjectItem : Entity
    {
        public struct Fields
        {
            public const string NewName = "new_name";
            public const string NewProjectItemId = "new_project_itemid";
            public const string Id = "new_project_itemid";
            public const string NewProjectId = "new_projectid";
        }

        public const string EntityLogicalName = "new_project_item";

        public ProjectItem() : base(EntityLogicalName)
        {
        }

        /// <summary>
        /// The name of the custom entity.
        /// </summary>
        [AttributeLogicalNameAttribute("new_name")]
        public string Name
        {
            get { return GetAttributeValue<string>("new_name"); }
            set { SetAttributeValue("new_name", value); }
        }

        /// <summary>
        /// Уникальный идентификатор экземпляров сущности
        /// </summary>
        [AttributeLogicalNameAttribute("new_project_itemid")]
        public Guid? NewProjectItemId
        {
            get { return GetAttributeValue<Guid?>("new_project_itemid"); }
            set
            {
                SetAttributeValue("new_project_itemid", value);
                base.Id = value ?? Guid.Empty;
            }
        }

        [AttributeLogicalName("new_project_itemid")]
        public override Guid Id
        {
            get { return base.Id; }
            set { NewProjectItemId = value; }
        }
    }
}