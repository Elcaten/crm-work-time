﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;

namespace Lineyschikov.Andrew.CrmWorkTime.Entities
{
    /// <summary>
    /// Рабочий день
    /// </summary>
    [DataContract]
    [EntityLogicalName("new_timesheet")]
    public class TimeSheet : Entity
    {
        public struct Fields
        {
            public const string CreatedBy = "createdby";
            public const string CreatedOn = "createdon";
            public const string CreatedOnBehalfBy = "createdonbehalfby";
            public const string ImportSequenceNumber = "importsequencenumber";
            public const string ModifiedBy = "modifiedby";
            public const string ModifiedOn = "modifiedon";
            public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
            public const string NewDate = "new_date";
            public const string NewFrom = "new_from";
            public const string NewName = "new_name";
            public const string NewPause = "new_pause";
            public const string NewTill = "new_till";
            public const string NewTimesheetId = "new_timesheetid";
            public const string Id = "new_timesheetid";
            public const string OverriddenCreatedOn = "overriddencreatedon";
            public const string OwnerId = "ownerid";
            public const string OwningBusinessUnit = "owningbusinessunit";
            public const string OwningTeam = "owningteam";
            public const string OwningUser = "owninguser";
            public const string Statecode = "Statecode";
            public const string Statuscode = "Statuscode";
            public const string TimeZoneRuleVersionNumber = "timezoneruleversionnumber";
            public const string UtcConversionTimeZoneCode = "utcconversiontimezonecode";
            public const string VersionNumber = "versionnumber";
            public const string NewTimesheetTimesheetItem = "new_new_timesheet_new_timesheet_item";
        }

        public const string EntityLogicalName = "new_timesheet";

        public TimeSheet() : base(EntityLogicalName)
        {
        }

        /// <summary>
        /// Дата
        /// </summary>
        [AttributeLogicalName("new_date")]
        public DateTime? Date
        {
            get { return GetAttributeValue<DateTime?>("new_date"); }
            set { SetAttributeValue("new_date", value); }
        }

        /// <summary>
        /// Время прихода
        /// </summary>
        [AttributeLogicalNameAttribute("new_from")]
        public DateTime? TimeFrom
        {
            get { return GetAttributeValue<DateTime?>("new_from"); }
            set { SetAttributeValue("new_from", value); }
        }

        /// <summary>
        /// The name of the custom entity.
        /// </summary>
        [AttributeLogicalNameAttribute("new_name")]
        public string NewName
        {
            get { return GetAttributeValue<string>("new_name"); }
            set { SetAttributeValue("new_name", value); }
        }

        /// <summary>
        /// Обед (в минутах)
        /// </summary>
        [AttributeLogicalNameAttribute("new_pause")]
        public int? Pause
        {
            get { return GetAttributeValue<int?>("new_pause"); }
            set { SetAttributeValue("new_pause", value); }
        }

        /// <summary>
        /// Время ухода
        /// </summary>
        [AttributeLogicalNameAttribute("new_till")]
        public DateTime? TimeTo
        {
            get { return GetAttributeValue<DateTime?>("new_till"); }
            set { SetAttributeValue("new_till", value); }
        }

        /// <summary>
        /// Уникальный идентификатор экземпляров сущности
        /// </summary>
        [AttributeLogicalNameAttribute("new_timesheetid")]
        public Guid? TimesheetId
        {
            get { return GetAttributeValue<Guid?>("new_timesheetid"); }
            set
            {
                SetAttributeValue("new_timesheetid", value);
                base.Id = value ?? Guid.Empty;
            }
        }

        [AttributeLogicalNameAttribute("new_timesheetid")]
        public override Guid Id
        {
            get { return base.Id; }
            set { TimesheetId = value; }
        }

        /// <summary>
        /// Элементы рабочего дня
        /// </summary>
        [RelationshipSchemaNameAttribute("new_new_timesheet_new_timesheet_item")]
        public IEnumerable<TimesheetItem> TimesheetItems
        {
            get { return GetRelatedEntities<TimesheetItem>("new_new_timesheet_new_timesheet_item", null); }
            set { SetRelatedEntities("new_new_timesheet_new_timesheet_item", null, value); }
        }
    }
}