﻿using System;
using System.Runtime.Serialization;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;

namespace Lineyschikov.Andrew.CrmWorkTime.Entities
{
    /// <summary>
    /// Элемент рабочего дня
    /// </summary>
    [DataContract]
    [EntityLogicalName("new_timesheet_item")]
    public class TimesheetItem : Entity
    {
        public struct Fields
        {
            public const string CreatedBy = "createdby";
            public const string CreatedOn = "createdon";
            public const string CreatedOnBehalfBy = "createdonbehalfby";
            public const string ImportSequenceNumber = "importsequencenumber";
            public const string ModifiedBy = "modifiedby";
            public const string ModifiedOn = "modifiedon";
            public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
            public const string NewInitiator = "new_initiator";
            public const string NewName = "new_name";
            public const string NewProject = "new_project";
            public const string NewProjectItem = "new_project_item";
            public const string NewTime = "new_time";
            public const string NewTimesheetItemId = "new_timesheet_itemid";
            public const string Id = "new_timesheet_itemid";
            public const string NewTimesheetId = "new_timesheetid";
            public const string NewTxt = "new_txt";
            public const string OverriddenCreatedOn = "overriddencreatedon";
            public const string OwnerId = "ownerid";
            public const string OwningBusinessUnit = "owningbusinessunit";
            public const string OwningTeam = "owningteam";
            public const string OwningUser = "owninguser";
            public const string Statecode = "Statecode";
            public const string Statuscode = "Statuscode";
            public const string TimeZoneRuleVersionNumber = "timezoneruleversionnumber";
            public const string UtcConversionTimeZoneCode = "utcconversiontimezonecode";
            public const string VersionNumber = "versionnumber";
            public const string NewNewProjectItemNewTimesheetItemProjectItem = "new_project_item";
            public const string NewNewProjectNewTimesheetItemNull = "new_project";
            public const string NewNewTimesheetNewTimesheetItem = "new_timesheetid";
        }

        public const string EntityLogicalName = "new_timesheet_item";

        public TimesheetItem() : base(EntityLogicalName)
        {
        }

        /// <summary>
        /// Уникальный идентификатор для объекта Проект, связанного с объектом Выполненная работа.
        /// </summary>
        [AttributeLogicalNameAttribute("new_project")]
        public EntityReference Project
        {
            get { return GetAttributeValue<EntityReference>("new_project"); }
            set { SetAttributeValue("new_project", value); }
        }

        /// <summary>
        /// Уникальный идентификатор для объекта Задача проекта, связанного с объектом Выполненная работа.
        /// </summary>
        [AttributeLogicalName("new_project_item")]
        public EntityReference ProjectItem
        {
            get { return GetAttributeValue<EntityReference>("new_project_item"); }
            set { SetAttributeValue("new_project_item", value); }
        }

        /// <summary>
        /// Время
        /// </summary>
        [AttributeLogicalNameAttribute("new_time")]
        public double? Time
        {
            get { return GetAttributeValue<double?>("new_time"); }
            set { SetAttributeValue("new_time", value); }
        }

        /// <summary>
        /// Уникальный идентификатор экземпляров сущности
        /// </summary>
        [AttributeLogicalNameAttribute("new_timesheet_itemid")]
        public Guid? NewTimesheetItemId
        {
            get { return GetAttributeValue<Guid?>("new_timesheet_itemid"); }
            set
            {
                SetAttributeValue("new_timesheet_itemid", value);
                base.Id = value ?? Guid.Empty;
            }
        }

        [AttributeLogicalNameAttribute("new_timesheet_itemid")]
        public override Guid Id
        {
            get { return base.Id; }
            set { NewTimesheetItemId = value; }
        }

        /// <summary>
        /// Уникальный идентификатор для объекта Рабочий день, связанного с объектом Выполненная работа.
        /// </summary>
        [AttributeLogicalNameAttribute("new_timesheetid")]
        public EntityReference TimeheetId
        {
            get { return GetAttributeValue<EntityReference>("new_timesheetid"); }
            set { SetAttributeValue("new_timesheetid", value); }
        }

        /// <summary>
        /// Описание
        /// </summary>
        [AttributeLogicalNameAttribute("new_txt")]
        public string Text
        {
            get { return GetAttributeValue<string>("new_txt"); }
            set { SetAttributeValue("new_txt", value); }
        }
    }
}