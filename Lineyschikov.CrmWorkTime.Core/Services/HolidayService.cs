﻿using System;
using System.Net;
using Newtonsoft.Json.Linq;

namespace Lineyschikov.Andrew.CrmWorkTime
{
    /// <summary>
    /// Сервис определения выходных и праздничных дней
    /// </summary>
    internal static class HolidayService
    {
        private const string HolidayCode = "2";
        private const string ApiUrl = "http://basicdata.ru/api/json/calend/";
        private static readonly JObject HolidayData = null;

        static HolidayService()
        {
            using (var client = new WebClient())
            {
                try
                {
                    HolidayData = JObject.Parse(client.DownloadString(ApiUrl));
                }
                catch (WebException)
                {
                }                
            }
        }

        /// <summary>
        /// Возвращает true, если день - выходной или праздник, иначе - false
        /// </summary>
        public static bool IsHoliday(DateTime date)
        {
            if (HolidayData == null) return false;
            var day = HolidayData?.SelectToken("data")
                ?.SelectToken(date.Year.ToString())
                ?.SelectToken(date.Month.ToString())
                ?.SelectToken(date.Day.ToString());
            var dayCode = day?.SelectToken("isWorking")?.ToString();
            var isHoliday = dayCode == HolidayCode;
            return isHoliday || IsWeekend(date);
        }

        private static bool IsWeekend(DateTime date)
        {
            return date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday;
        }
    }
}