﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using LibGit2Sharp;
using Lineyschikov.Andrew.CrmWorkTime.Entities;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;

namespace Lineyschikov.Andrew.CrmWorkTime
{
    /// <summary>
    /// Сервис обработки рабочих дней
    /// </summary>
    public class TimeSheetService
    {
        private readonly OrganizationServiceContext _context;
        private readonly string _taskName;
        private readonly string _repositoryPath;
        private readonly string _authorName;

        /// <summary>
        /// Создает экзмепляр сервиса обработки рабочих дней
        /// </summary>
        /// <param name="context">CRM контекст</param>
        /// <param name="taskName">Название проекта</param>
        /// <param name="repositoryPath">Полный путь к репозиторию</param>
        /// <param name="authorName">Автор коммитов</param>
        public TimeSheetService(OrganizationServiceContext context, string taskName, string repositoryPath, string authorName)
        {
            _context = context;
            _taskName = taskName;
            _repositoryPath = repositoryPath;
            _authorName = authorName;
        }

        #region Public Methods
        /// <summary>
        /// Создает модель рабочего дня на основе коммитов
        /// </summary>
        /// <param name="date">Дата рабочего дня</param>
        public TimeSheetModel GenerateTimesheet(DateTime date)
        {
            var commits = new Repository(_repositoryPath).Branches.SelectMany(s => s.Commits);
            var timeSheetItemModels = commits
                .Where(c => c.Author.Name == _authorName && c.Author.When.Date == date.Date)
                .Distinct(new CommitMessageComparer())
                .Select(c => new TimeSheetItemModel(c.MessageShort, 0));
            return new TimeSheetModel(date, timeSheetItemModels.ToList());
        }

        /// <summary>
        /// Добавляет или обновляет рабочий день в CRM
        /// </summary>
        /// <param name="timeSheetModel">Модель рабочего дня</param>
        public void AddOrUpdateTimeSheet(TimeSheetModel timeSheetModel)
        {
            DeleteTimeSheet(timeSheetModel.Date);
            AddTimeSheet(timeSheetModel);
            _context.SaveChanges();
        }

        /// <summary>
        /// Получает даты рабочих дней, незаполненных за месяц
        /// </summary>
        /// <param name="date">Дата</param>
        /// <param name="incldeWeekend">true для включения выходных, иначе - false</param>
        public IEnumerable<DateTime> GetPendingDates(DateTime date, bool incldeWeekend = false)
        {
            var existingDates = GetExisitingDates(date);
            var allDates = new List<DateTime>();
            for (var i = 1; i <= date.Day; i++)
            {
                var dateToAdd = new DateTime(date.Year, date.Month, i);
                if (!HolidayService.IsHoliday(dateToAdd) || incldeWeekend) allDates.Add(dateToAdd);   
            }
            return allDates.Except(existingDates);
        }
        #endregion Public Methods

        #region Private Methods
        /// <summary>
        /// Получает даты рабочих дней в том же месяце
        /// </summary>
        /// <param name="date">Дата</param>
        private IEnumerable<DateTime> GetExisitingDates(DateTime date)
        {
            return _context.CreateQuery<TimeSheet>()
                .Where(ts => ts.Date >= new DateTime(date.Year, date.Month, 1) && ts.Date < date.AddDays(1))
                .Where(ts => ts.Date != null)
                .Select(ts => ts.Date.Value.Date);
        }

        /// <summary>
        /// Удаляет рабочий день из CRM
        /// </summary>
        /// <param name="date">Дата рабочего дня</param>
        private void DeleteTimeSheet(DateTime date)
        {
            var crmTimeSheet = _context.CreateQuery<TimeSheet>().FirstOrDefault(t => t.Date == date);
            if (crmTimeSheet == null) return;
            var crmTimeSheetItems = _context.CreateQuery<TimesheetItem>()
                .Where(ti => ti.TimeheetId != null && ti.TimeheetId.Id == crmTimeSheet.TimesheetId.Value)
                .ToList();
            if (crmTimeSheetItems.Any())
            {
                foreach (var crmTimeSheetItem in crmTimeSheetItems)
                {
                    _context.DeleteObject(crmTimeSheetItem, true);
                }
            }
            _context.DeleteObject(crmTimeSheet);
        }

        /// <summary>
        /// Добавляет рабочий день в CRM
        /// </summary>
        /// <param name="timeSheetModel">Модель рабочего дня</param>
        private void AddTimeSheet(TimeSheetModel timeSheetModel)
        {
            var gmcsProject = _context.CreateQuery<Project>().FirstOrDefault(p => p.Name == Constants.Projects.GMCS);
            var userProjectItem = _context.CreateQuery<ProjectItem>().FirstOrDefault(pi => pi.Name == _taskName);
            var downTimeProjectItem = _context.CreateQuery<ProjectItem>().FirstOrDefault(pi => pi.Name == Constants.ProjectItems.DownTime);
            if(gmcsProject == null || userProjectItem == null || downTimeProjectItem == null) throw new Exception("Не удалось найти сущность в CRM");
            var timeSheetItems = timeSheetModel.Items
                .Where(ci => ci.HoursSpent > 0)
                .Select(ci =>
                    new TimesheetItem
                    {
                        Text = ci.Name,
                        Time = ci.HoursSpent,
                        Project = gmcsProject.ToEntityReference(),
                        ProjectItem = ci.IsDonwTime ? downTimeProjectItem.ToEntityReference() : userProjectItem.ToEntityReference(),
                    }
                );
            var timeSheetDate = timeSheetModel.Date;
            var timeSheet = new TimeSheet
            {
                TimesheetItems = timeSheetItems,
                Date = timeSheetDate,
                TimeFrom = timeSheetDate.AddHours(Constants.Hours.Start),
                TimeTo = timeSheetDate.AddHours(Constants.Hours.Shutdown),
                Pause = 60,
            };
            _context.AddObject(timeSheet);
        }
        #endregion Private Methods
    }
}