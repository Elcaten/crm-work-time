﻿using System;
using System.Net;
using System.ServiceModel.Description;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk.Client;

namespace Lineyschikov.Andrew.CrmWorkTime
{
    /// <summary>
    /// Фабрика OrganizationServiceContext
    /// </summary>
    internal class ContextFactory : IFactory
    {
        /// <summary>
        /// Создает экземпляр OrganizationServiceContext
        /// </summary>
        public static OrganizationServiceContext GetContext(string crmServiceUrl)
        {
            var credentials = new ClientCredentials();
            credentials.Windows.ClientCredential = CredentialCache.DefaultNetworkCredentials;
            var organizationUri = new Uri(crmServiceUrl);
            var serviceProxy = new OrganizationServiceProxy(organizationUri, null, credentials, null);
            serviceProxy.EnableProxyTypes();
            return new CrmOrganizationServiceContext(serviceProxy);
        }
    }
}