﻿using System;
using System.Net;
using System.ServiceModel.Description;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;

namespace Lineyschikov.Andrew.CrmWorkTime
{
    /// <summary>
    /// Фабрика IOrganizationService
    /// </summary>
    internal class OrganizationServiceFactory : IFactory
    {
        /// <summary>
        /// Создает экземпляр IOrganizationService
        /// </summary>
        public static IOrganizationService GetService(string crmServiceUrl)
        {
            var credentials = new ClientCredentials();
            credentials.Windows.ClientCredential = CredentialCache.DefaultNetworkCredentials;
            var organizationUri = new Uri(crmServiceUrl);
            var serviceProxy = new OrganizationServiceProxy(organizationUri, null, credentials, null);
            serviceProxy.EnableProxyTypes();
            return serviceProxy;
        } 
    }
}