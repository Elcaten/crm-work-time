﻿using Lineyschikov.Andrew.CrmWorkTime.Entities;

namespace Lineyschikov.Andrew.CrmWorkTime
{
    /// <summary>
    /// Модель элемента рабочего дня
    /// </summary>
    public class TimeSheetItemModel
    {
        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Число потраченных часов
        /// </summary>
        public double HoursSpent { get; set; }

        public bool IsDonwTime { get; set; }

        /// <summary>
        /// Создает экземпляр модели элементов рабочего дня
        /// </summary>
        /// <param name="name">Название</param>
        /// <param name="hoursSpent">Число потраченных часов</param>
        /// <param name="isDonwTime">true - если простой, иначе - false</param>
        public TimeSheetItemModel(string name, double hoursSpent, bool isDonwTime = false)
        {
            Name = name;
            HoursSpent = hoursSpent;
            IsDonwTime = isDonwTime;
        }

        /// <summary>
        /// Создает экземпляр модели элементов рабочего дня
        /// </summary>
        /// <param name="timesheetItem">Элемент рабочего дня</param>
        internal TimeSheetItemModel(TimesheetItem timesheetItem, bool isDonwTime = false)
        {
            IsDonwTime = isDonwTime;
            Name = timesheetItem.Text;
            HoursSpent = timesheetItem.Time ?? 0;
        }
    }
}