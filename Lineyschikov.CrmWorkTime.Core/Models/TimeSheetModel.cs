﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lineyschikov.Andrew.CrmWorkTime.Entities;

namespace Lineyschikov.Andrew.CrmWorkTime
{
    /// <summary>
    /// Модель рабочего дня
    /// </summary>
    public class TimeSheetModel
    {
        /// <summary>
        /// Дата
        /// </summary>
        public DateTime Date { get; set; }
        
        /// <summary>
        /// true, если рабочий день 8-часовой, иначе - false
        /// </summary>
        public bool IsEightHoursDay { get { return new PrecisionComparer().Equals(Items.Select(i => i.HoursSpent).Sum(), 8.0); } }

        /// <summary>
        /// Элементы рабочего дня
        /// </summary>
        public IList<TimeSheetItemModel> Items { get; set; }

        /// <summary>
        /// Создает экземпляр модели рабочего дня
        /// </summary>
        /// <param name="date">Дата</param>
        /// <param name="items">Элементы рабочего дня</param>
        public TimeSheetModel(DateTime date, IList<TimeSheetItemModel> items)
        {
            Date = date;
            Items = items;
        }

        /// <summary>
        /// Создает экземпляр модели рабочего дня
        /// </summary>
        /// <param name="timeSheet">Рабочий день</param>
        internal TimeSheetModel(TimeSheet timeSheet)
        {
            Date = timeSheet.Date ?? DateTime.MinValue;
            Items = timeSheet.TimesheetItems == null
                ? new List<TimeSheetItemModel>()
                : timeSheet.TimesheetItems.Select(i => new TimeSheetItemModel(i)).ToList();
        }
    }
}