﻿using System.Configuration;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using static System.Configuration.ConfigurationManager;

namespace Lineyschikov.Andrew.CrmWorkTime.Windsor
{
    /// <summary>
    /// Регистрация компонентов CrmWorkTime.Core
    /// </summary>
    public class CoreInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var crmServiceUrl = AppSettings["crmServiceUrl"];
            container.AddFacility<TypedFactoryFacility>();
            container.Register(
                Classes.FromAssemblyInThisApplication().BasedOn<IFactory>(),                   
                Component.For<IOrganizationService>().UsingFactoryMethod(f => OrganizationServiceFactory.GetService(crmServiceUrl)),
                Component.For<OrganizationServiceContext>().UsingFactoryMethod(f => ContextFactory.GetContext(crmServiceUrl)),
                Component.For<TimeSheetService>()
                    .DependsOn(Dependency.OnAppSettingsValue("taskName", "taskName"))
                    .DependsOn(Dependency.OnAppSettingsValue("repositoryPath", "repositoryPath"))
                    .DependsOn(Dependency.OnAppSettingsValue("authorName", "authorName"))
            );
        }
    }
}