﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Castle.Windsor;
using Lineyschikov.Andrew.CrmWorkTime.Windsor;

namespace Lineyschikov.Andrew.CrmWorkTime.Ui
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private IWindsorContainer _container;
        private static readonly string _nikfortVpnName = ConfigurationManager.AppSettings["nikfortVpnName"];

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            VpnManager.Connect(_nikfortVpnName);
            _container = new WindsorContainer();
            _container.Install(new CoreInstaller(), new GuiInstaller());
            var mainWindow = _container.Resolve<IMainWindowView>();
            mainWindow.Show();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            _container.Dispose();
            VpnManager.Disconnect(_nikfortVpnName);
        }

        private void App_OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.Message, "Ошибка!");
            e.Handled = true;
        }
    }
}
