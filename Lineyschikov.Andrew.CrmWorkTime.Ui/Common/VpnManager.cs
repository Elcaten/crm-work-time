using System.Diagnostics;

namespace Lineyschikov.Andrew.CrmWorkTime.Ui
{
    public static class VpnManager
    {
        public static void Connect(string vpnName)
        {
            var process = Process.Start("rasdial.exe", vpnName);
            if (process != null) process.WaitForExit();
        }

        public static void Disconnect(string vpnName)
        {
            Process.Start("rasdial.exe", vpnName + " /disconnect");
        }
    }
}