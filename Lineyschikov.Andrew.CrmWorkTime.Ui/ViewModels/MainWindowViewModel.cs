using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Lineyschikov.Andrew.CrmDeploy.UI.ViewModels;
using Lineyschikov.WpfAssignment.Desktop.Commands;

namespace Lineyschikov.Andrew.CrmWorkTime.Ui
{
    public class MainWindowViewModel  : ViewModelBase
    {
        private readonly TimeSheetService _timeSheetService;
        private DateTime? _selectedDate;

        public ObservableCollection<TimeSheetItemModel> Tasks { get; set; }
        public DateTime? SelectedDate
        {
            get { return _selectedDate; }
            set
            {
                SetAndNotify(ref _selectedDate, value);
                if (!_selectedDate.HasValue) return;
                Tasks.Clear();
                foreach (var timeSheetItemModel in _timeSheetService.GetTimeSheet(_selectedDate.Value).Items)
                {
                    Tasks.Add(timeSheetItemModel);
                }
            }
        }

        public ICommand SaveTasksCommand { get; set; }
        public ICommand CheckTasksForMonth { get; set; }
        public ICommand CreateTimeSheetCommand { get; set; }

        public MainWindowViewModel()
        {
        }

        public MainWindowViewModel(TimeSheetService timeSheetService)
        {
            _timeSheetService = timeSheetService;
            Tasks = new ObservableCollection<TimeSheetItemModel>();
            CreateTimeSheetCommand = new SimpleCommand(() =>
            {
                if(SelectedDate == null) return;
                Tasks.Clear();
                var items = _timeSheetService.CreateTimesheet(SelectedDate.Value).Items;
                foreach (var timeSheetItemModel in items)
                {
                    Tasks.Add(timeSheetItemModel);
                }
            });
         
            SaveTasksCommand = new SimpleCommand(() =>
            {
                if (SelectedDate == null) return;
                _timeSheetService.AddOrUpdateTimeSheet(new TimeSheetModel(SelectedDate.Value, Tasks));
            });
        }
    }

}