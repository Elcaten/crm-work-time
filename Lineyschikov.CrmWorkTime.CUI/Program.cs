﻿using System;
using System.Configuration;
using System.Net;
using System.Text.RegularExpressions;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers;
using Castle.Windsor;
using Feonufry.CUI.Menu.Builders;
using Lineyschikov.Andrew.CrmWorkTime;
using Lineyschikov.Andrew.CrmWorkTime.Windsor;
using Lineyschikov.VpnManager;

namespace Lineyschikov.CrmWorkTime.CUI
{
    public class Program
    {
        public Program(TimeSheetMenuActions menuActions)
        {
            menuActions.ManageTimeSheets(DateTime.Today);
        }

        public static void Main()
        {
            IVpnManager vpn = null;
            try
            {
                var container = CreateContainer();
                vpn = container.Resolve<IVpnManager>();
                vpn.Connect();
                container.Resolve<Program>();
            }
            catch (DependencyResolverException e)
            {
                WriteError("Не удалось запустить программу. Ошибка в файле конфигурации:", Regex.Match(e.Message, "Parameter.*").Value);
            }
            catch (VpnException e)
            {
                WriteError("Ошибка VPN подключения:", e.Message);
            }
            catch (InvalidOperationException e)
            {
                if (e.InnerException?.GetType() == typeof (WebException))
                {
                    WriteError("Не удалось подключиться к CRM:", e.Message);
                }
            }
            catch (Exception e)
            {
                WriteError("Неожиданная ошибка:", e.Message);
            }
            finally
            {
                vpn?.Disconnect();
            }
        }

        private static IWindsorContainer CreateContainer()
        {
            var includeWeekend = bool.Parse(ConfigurationManager.AppSettings["includeWeekend"]);
            var container = new WindsorContainer();
            container.Install(new CoreInstaller());
            container.Register(
                Component.For<IVpnManager>()
                    .ImplementedBy<WindowsVpnManager>()
                    .DependsOn(Dependency.OnAppSettingsValue("vpnName", "nikfortVpnName")),
                Component.For<TimeSheetMenuActions>()
                    .DependsOn(Dependency.OnValue("includeWeekend", includeWeekend)),
                Component.For<Program>()
            );
            return container;
        }

        private static void WriteError(string title, string message)
        {
            Console.Error.WriteLine(title);
            Console.Error.WriteLine(message);
            Console.Error.WriteLine("Нажмите любую клавишу...");
            Console.ReadKey();
        }
    }
}
