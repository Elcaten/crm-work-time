﻿using System;
using System.Linq;
using System.Text;
using Feonufry.CUI.Actions;
using Feonufry.CUI.Menu.Builders;
using Lineyschikov.Andrew.CrmWorkTime;

namespace Lineyschikov.CrmWorkTime.CUI
{
    /// <summary>
    /// Пункты меню рабочих дней
    /// </summary>
    public class TimeSheetMenuActions
    {
        private struct MenuTitles
        {
            public const string Exit = "Выход";
            public const string Cancel = "Отмена";
        }

        private readonly TimeSheetService _service;
        private readonly bool _includeWeekend;

        public TimeSheetMenuActions(TimeSheetService service, bool includeWeekend)
        {
            _service = service;
            _includeWeekend = includeWeekend;
        }

        /// <summary>
        /// Создает меню выбора неазаполненных рабочих дней
        /// </summary>
        public void ManageTimeSheets(DateTime maxDate)
        {
            Console.Clear();
            var menu = new MenuBuilder()
                .Title("Незаполненные даты:")
                .Prompt("Выберите дату:")
                .WrongCommandMessage("Неправильное дата!");
            var pendingDates = _service.GetPendingDates(maxDate, _includeWeekend);
            foreach (var requiredDate in pendingDates)
            {
                menu.Item(requiredDate.ToString("m"), ctx => ManageTimeSheet(ctx, requiredDate));
            }
            menu.Exit(MenuTitles.Exit);
            menu.GetMenu().Run();
        }

        private void ManageTimeSheet(ActionExecutionContext ctx, DateTime requiredDate)
        {
            Console.Clear();
            var timeSheet = _service.GenerateTimesheet(requiredDate);
            new MenuBuilder()
                .Title($"Задачи за {requiredDate}")
                .Prompt("Выберите действие:")
                .WrongCommandMessage("Неправильное действие!")
                    .Item("Добавить", c => AddTimeSheetItem(c, timeSheet))
                    .Item("Редактировать", c => EditTimeSheetItem(c, timeSheet))
                    .Item("Удалить", c => DeleteTimeSheetItem(c, timeSheet))
                    .Item("Завершить", c => SendToCrm(c, timeSheet))
                    .Exit(MenuTitles.Cancel)
                .GetMenu().Run();
        }

        /// <summary>
        /// Сохраняет модлеь рабочего дня в CRM
        /// </summary>
        private void SendToCrm(ActionExecutionContext ctx, TimeSheetModel timeSheet)
        {
            if (timeSheet.IsEightHoursDay || PromptConfirmation(ctx, "Сумма часов не равна 8. Вы уверены?"))
            {
                _service.AddOrUpdateTimeSheet(timeSheet);
            }
        }

        private static void AddTimeSheetItem(ActionExecutionContext ctx, TimeSheetModel timeSheet)
        {
            Console.Clear();
            new MenuBuilder()
                .Title("Добавить задачу")
                .Prompt("Выберите задачу:")
                .WrongCommandMessage("Неправильная задача!")
                .RunnableOnce()
                    .Item("Задача", c =>
                    {
                        timeSheet.Items.Add(new TimeSheetItemModel(PromptTitle(ctx, ""), PromptWorkingHours(ctx, "")));
                    })
                    .Item("Простой", c =>
                    {
                        timeSheet.Items.Add(new TimeSheetItemModel("Ожидание задач", PromptWorkingHours(ctx, ""), true));
                    })
                    .Exit(MenuTitles.Cancel)
                .GetMenu().Run();
        }

        private static void DeleteTimeSheetItem(ActionExecutionContext ctx, TimeSheetModel timeSheet)
        {
            Console.Clear();
            var menu = new MenuBuilder()
                .Title("Удалить задачу")
                .Prompt("Выберите задачу:")
                .WrongCommandMessage("Неправильная задача!")
                .RunnableOnce();
            for (int i = 0; i < timeSheet.Items.Count; i++)
            {
                var currentItemIndex = i;
                var item = timeSheet.Items[currentItemIndex];
                menu.Item(
                    $"{Truncate(item.Name, 50)} | {item.HoursSpent}ч",
                    c => { timeSheet.Items.RemoveAt(currentItemIndex); }
                );
            }
            menu.Exit(MenuTitles.Cancel)
            .GetMenu().Run();
        }

        private static void EditTimeSheetItem(ActionExecutionContext ctx, TimeSheetModel timeSheet)
        {
            Console.Clear();
            var menu = new MenuBuilder()
                .Title("Редактировать задачу")
                .Prompt("Выберите задачу:")
                .WrongCommandMessage("Неправильная задача!")
                .RunnableOnce();
            for (int i = 0; i < timeSheet.Items.Count; i++)
            {
                var currentItemIndex = i;
                var item = timeSheet.Items[currentItemIndex];
                menu.Item(
                    $"{Truncate(item.Name, 50)} | {item.HoursSpent}ч",
                    c => { timeSheet.Items[currentItemIndex].HoursSpent = PromptWorkingHours(ctx, ""); }
                );
            }
            menu.Exit(MenuTitles.Cancel)
            .GetMenu().Run();
        }

        /// <summary>
        /// Запрашивает число часов
        /// </summary>
        private static double PromptWorkingHours(ActionExecutionContext context, string title)
        {
            context.Out.WriteLine(title);
            context.Out.Write("Введите количество часов > ");
            double hours;
            while (!double.TryParse(context.In.ReadLine(), out hours))
            {
                context.Out.WriteLine("Ошибка! Введите количество часов > ");
            }
            return hours;
        }

         /// <summary>
        /// Запрашивает название задачи
        /// </summary>
        private static string PromptTitle(ActionExecutionContext context, string title)
        {
            if(!string.IsNullOrEmpty(title)) context.Out.WriteLine(title);
            context.Out.Write("Введите название задачи > ");
            return context.In.ReadLine();
        }

        /// <summary>
        /// Запрашивает подтверждение
        /// </summary>
        private static bool PromptConfirmation(ActionExecutionContext context, string title)
        {
            if(!string.IsNullOrEmpty(title)) context.Out.WriteLine(title);
            context.Out.Write("д/н (y/n) > ");
            string input = context.In.ReadLine();
            return input == "д" || input == "y";
        }

        private static string Truncate(string value, int maxChars)
        {
            return value.Length <= maxChars ? value : value.Substring(0, maxChars) + "...";
        }
    }
}